<?php
/**
 * @file
 * The primary PHP file for this theme.
 *
 * @see template_preprocess_node()
 */
function cpedtheme_preprocess_node(&$variables) {
    $node = $variables['elements']['#node'];
    if (module_exists('contact')) {
        $contact_form = drupal_get_form('contact_site_form');
        $contact_form['subject']['#value'] = '勘誤回報 (提醒管理員): '.$node->title;
        $variables['contact_form'] = render($contact_form);
    }
}

function cpedtheme_preprocess_page(&$variables) {

    // Primary nav.
    $variables['primary_mobile_nav'] = FALSE;
    if ($variables['main_menu']) {
        // Build links.
        $variables['primary_mobile_nav'] = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
        // Provide default theme wrapper function.
        $variables['primary_mobile_nav']['#theme_wrappers'] = array('menu_tree__primary');
    }

    // Secondary nav.
    $variables['secondary_mobile_nav'] = FALSE;
    if ($variables['secondary_menu']) {
        // Build links.
        $variables['secondary_mobile_nav'] = menu_tree('menu-worklist');
        // Provide default theme wrapper function.
        $variables['secondary_mobile_nav']['#theme_wrappers'] = array('menu_tree__secondary');
    }
}

function cpedtheme_menu_link(array $variables) {

    $element = $variables['element'];
    $sub_menu = '';

    $title = $element['#title'];
    $href = $element['#href'];
    $options = !empty($element['#localized_options']) ? $element['#localized_options'] : array();
    $attributes = !empty($element['#attributes']) ? $element['#attributes'] : array();
    if ($element['#below']) {
        // Prevent dropdown functions from being added to management menu so it
        if ($element['#original_link']['menu_name'] == 'navigation' || $element['#original_link']['menu_name'] ==  'menu-worklist') {

            // Get id for the collapse div element
            $collapse_id = $element['#original_link']['mlid'];

            //// Add our own wrapper.
            // Wrap collapse div to submenu
            unset($element['#below']['#theme_wrappers']);
            if(in_array('active', $attributes['class'])) {
                $sub_menu = '<div class="collapse in" id="collapse-' . $collapse_id . '" aria-expanded="true"><ul>' . drupal_render($element['#below']) . '</ul></div>';
            }
            else {
                $sub_menu = '<div class="collapse" id="collapse-' . $collapse_id . '"><ul>' . drupal_render($element['#below']) . '</ul></div>';
            }

            // Generate as standard dropdown.
            $title .= ' <span class="caret"></span>';
            $attributes['class'][] = 'collapse';

            $options['html'] = TRUE;

            //// Set dropdown trigger element to # to prevent inadvertant page loading
            // Change dropdown to collapse
            $options['attributes']['data-toggle'] = 'collapse';

            // Set href target to anchor collapse div
            $href = '';
            $options['fragment'] = 'collapse-' . $collapse_id;
            $options['external'] = TRUE;
        }
        elseif ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] == 1)) {
            // Add our own wrapper.
            unset($element['#below']['#theme_wrappers']);
            $sub_menu = '<ul class="dropdown-menu">' . drupal_render($element['#below']) . '</ul>';

            // Generate as standard dropdown.
            $title .= ' <span class="caret"></span>';
            $attributes['class'][] = 'dropdown';

            $options['html'] = TRUE;

            // Set dropdown trigger element to # to prevent inadvertant page loading
            // when a submenu link is clicked.
            $options['attributes']['data-target'] = '#';
            $options['attributes']['class'][] = 'dropdown-toggle';
            $options['attributes']['data-toggle'] = 'dropdown';
        }
    }

    // Filter the title if the "html" is set, otherwise l() will automatically
    // sanitize using check_plain(), so no need to call that here.
    if (!empty($options['html'])) {
        $title = _bootstrap_filter_xss($title);
    }

    return '<li' . drupal_attributes($attributes) . '>' . l($title, $href, $options) . $sub_menu . "</li>\n";
}

function cpedtheme_theme() {
    return array(
        'resume_node_form' => array(
            'template' => 'resume-node-form',
            'render element' => 'form',
        ),
    );
}

/**
 * Implements hook_preprocess_HOOK().
 * Preprocessor for theme('resume_node_form').
 */
function cpedtheme_preprocess_resume_node_form(&$variables)
{
    $form = &$variables['form'];

    $form['group_personal_data']['field_re_pic']['#attributes']['class'][] = 'col-sm-12';
    $form['group_personal_data']['field_re_sex']['#attributes']['class'][] = 'col-sm-6';
    $form['group_personal_data']['field_re_race']['#attributes']['class'][] = 'col-sm-6';

    $form['taxonomy_vocabulary_2']['#attributes']['class'][] = 'col-sm-3';
    $form['taxonomy_vocabulary_3']['#attributes']['class'][] = 'col-sm-3';
    $form['taxonomy_vocabulary_5']['#attributes']['class'][] = 'col-sm-3';
    $form['taxonomy_vocabulary_6']['#attributes']['class'][] = 'col-sm-3';

    $form['group_personal_data']['field_re_birth_year']['#attributes']['class'][] = 'col-sm-3';
    $form['group_personal_data']['field_re_birth_month']['#attributes']['class'][] = 'col-sm-3';
    $form['group_personal_data']['field_re_dead_year']['#attributes']['class'][] = 'col-sm-3';
    $form['group_personal_data']['field_re_dead_month']['#attributes']['class'][] = 'col-sm-3';

    $form['group_personal_data']['field_re_degree']['#attributes']['class'][] = 'col-sm-4';
    $form['group_personal_data']['field_re_abroad_exp']['#attributes']['class'][] = 'col-sm-4';
    $form['group_personal_data']['field_re_abroad_country']['#attributes']['class'][] = 'col-sm-4';

    $form['group_personal_data']['field_re_sex']['#attributes']['class'][] = 'col-sm-6';
    $form['group_personal_data']['field_re_race']['#attributes']['class'][] = 'col-sm-6';

    $form['group_personal_data']['field_re_relative']['#attributes']['class'][] = 'col-sm-6';
    $form['group_personal_data']['field_re_relativetext']['#attributes']['class'][] = 'col-sm-6';

    $form['group_personal_data']['field_re_remark']['#attributes']['class'][] = 'col-sm-12';

    hide($form['group_personal_data']);
}

/**
 * Bootstrap theme wrapper function for the primary menu links.
 */
function cpedtheme_menu_tree__primary(&$variables) {
    return '<ul class="menu nav navbar-nav">' . $variables['tree'] . '</ul>';
}

/**
 * Bootstrap theme wrapper function for the secondary menu links.
 */
function cpedtheme_menu_tree__secondary(&$variables) {
    return '<ul class="menu nav navbar-nav secondary">' . $variables['tree'] . '</ul>';
}
