<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup templates
 */
?>

<?php if ($view_mode == 'full'): ?>
    <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    <?php if (isset($content['group_personal_data']['field_re_pic'])): ?>
        <div class="visible-xs-block">
            <?php print render($content['group_personal_data']['field_re_pic']); ?>
        </div>
    <?php endif; ?>
    <div class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title">個人資料</h3></div>
        <div class="table-responsive">
            <table class="table table-bordered" id="basic-info">
                <tr>
                    <?php if (isset($content['group_personal_data']['field_re_pic'])): ?>
                        <td rowspan='7' width="15%" class="text-center hidden-xs valign-middle">
                            <?php print render($content['group_personal_data']['field_re_pic']); ?>
                        </td>
                    <?php endif; ?>
                    <th>漢語拼音</th>
                    <td colspan="2">
                        <?php print render($content['group_personal_data']['field_re_han_name']); ?>
                    </td>
                    <th>性別</th>
                    <td colspan="2">
                        <?php print render($content['group_personal_data']['field_re_sex']); ?>
                    </td>
                </tr>
                <tr>
                    <th>年齡</th>
                    <td>
                        <div class="field-items">
                            <div class="field-item">
                                <?php
                                if ($content['group_personal_data']['field_re_dead_year']) {
                                    if ($content['group_personal_data']['field_re_birth_year'][0]['#markup']) {
                                        print $content['group_personal_data']['field_re_dead_year'][0]['#markup'] - $content['group_personal_data']['field_re_birth_year'][0]['#markup'];
                                    }
                                } else {
                                    if ($content['group_personal_data']['field_re_birth_year'][0]['#markup']) {
                                        print date('Y') - $content['group_personal_data']['field_re_birth_year'][0]['#markup'];
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </td>
                    <th>出生</th>
                    <td>
                        <div class="field-items">
                            <div class="field-item">
                                <?php
                                if (isset($content['group_personal_data']['field_re_birth_year'])) {
                                    print $content['group_personal_data']['field_re_birth_year'][0]['#markup'];
                                }
                                if (isset($content['group_personal_data']['field_re_birth_year']) && isset($content['group_personal_data']['field_re_birth_month'])) {
                                    print ' / ';
                                }
                                if (isset($content['group_personal_data']['field_re_birth_month'])) {
                                    print $content['group_personal_data']['field_re_birth_month'][0]['#markup'];
                                }
                                ?>
                            </div>
                        </div>
                    </td>
                    <th>死亡</th>
                    <td>
                        <div class="field-items">
                            <div class="field-item">
                                <?php
                                if (isset($content['group_personal_data']['field_re_dead_year'])) {
                                    print $content['group_personal_data']['field_re_dead_year'][0]['#markup'];
                                }
                                if (isset($content['group_personal_data']['field_re_dead_year']) && isset($content['group_personal_data']['field_re_dead_month'])) {
                                    print ' / ';
                                }
                                if (isset($content['group_personal_data']['field_re_dead_month'])) {
                                    print $content['group_personal_data']['field_re_dead_month'][0]['#markup'];
                                }
                                ?>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>最高學歷</th>
                    <td>
                        <?php print render($content['group_personal_data']['field_re_degree']); ?>
                    </td>
                    <th>留學經驗</th>
                    <td>
                        <?php print render($content['group_personal_data']['field_re_abroad_exp']); ?>
                    </td>
                    <th>留學國家</th>
                    <td>
                        <?php print render($content['group_personal_data']['field_re_abroad_country']); ?>
                    </td>
                </tr>
                <tr>

                    <th>民族</th>
                    <td>
                        <?php print render($content['group_personal_data']['field_re_race']); ?>
                    </td>
                    <th>祖籍</th>
                    <td>
                        <?php print render($content['taxonomy_vocabulary_2']); ?>
                        <?php print render($content['taxonomy_vocabulary_3']); ?>
                    </td>
                    <th>出生地</th>
                    <td>
                        <?php print render($content['taxonomy_vocabulary_5']); ?>
                        <?php print render($content['taxonomy_vocabulary_6']); ?>
                    </td>
                </tr>
                <tr>
                    <th>政黨</th>
                    <td>
                        <?php print render($content['group_personal_data']['field_re_party']); ?>
                    </td>
                    <th>專業頭銜</th>
                    <td>
                        <?php print render($content['group_personal_data']['field_re_main_title']); ?>
                    </td>
                    <th>特殊關係</th>
                    <td>
                        <?php print render($content['group_personal_data']['field_re_spe_relationship']); ?>
                    </td>
                </tr>
                <tr>
                    <th>入黨時間</th>
                    <td>
                        <?php print render($content['group_personal_data']['field_re_participant_time']); ?>
                    </td>
                    <th>參加工作</th>
                    <td>
                        <?php print render($content['group_personal_data']['field_re_work_time']); ?>
                    </td>
                    <th>共青團</th>
                    <td>
                        <?php print render($content['group_personal_data']['field_re_cyc']); ?>
                    </td>
                </tr>
                <tr>
                    <th>備註</th>
                    <td colspan=5>
                        <?php print render($content['group_personal_data']['field_re_remark']); ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered" id="relation-info">
                <tr class="bg-info">
                    <th width="50%">相關人物</th>
                    <th>關係</th>
                </tr>
                <?php if (isset($content['field_re_relationship'])): ?>
                    <?php foreach ($content['field_re_relationship']['#items'] as $item): ?>
                        <?php if (!empty($item['url']) && !empty($item['title'])): ?>
                            <tr>
                                <td>
                                    <div class="field-items">
                                        <div class="field-item">
                                            <a href="/search?name=<?php print $item['url'] ?>"><?php print $item['url'] ?></a>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="field-items">
                                        <div class="field-item">
                                            <?php print $item['title']; ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php endif ?>
                    <?php endforeach ?>
                <?php else: ?>
                    <tr>
                        <td colspan="2">
                            <div class="field-items">
                                <div class="field-item">資料庫無任何記錄</div>
                            </div>
                        </td>
                    </tr>
                <?php endif; ?>
            </table>
        </div>
    </div>
    <?php
    global $user;
    $permission = false;
    if (isset($user->roles[6]) || isset($user->roles[7]) || isset($user->roles[9]) || $user->uid == 1) {
        $permission = true;
    }
    ?>
    <div class="panel panel-success">
        <div class="panel-heading"><h3 class="panel-title">學歷資料</h3></div>
        <?php print views_embed_view('resume_exprience', 'block_3', $node->nid); ?>
    </div>
    <?php if ($permission): ?>
        <div id='manage-school' class='btn-group' role='group' aria-label=''>
            <a href='/school/<?php print $node->nid; ?>' class='btn btn-default'>學歷管理</a>
            <?php print $content['links']['flag']['#links']['flag-create_graduate_school']['title']; ?>
        </div>
    <?php endif; ?>
    <div class="panel panel-warning">
        <div class="panel-heading"><h3 class="panel-title">職務資料</h3></div>
            <?php
            print views_embed_view('resume_exprience', 'block_2', $node->nid);
            print views_embed_view('resume_exprience', 'block_1', $node->nid);
            ?>
    </div>
    <div id='manage-worktitle' class='btn-group' role='group' aria-label=''>
        <?php if ($permission): ?>
            <a href='/worktitle/<?php print $node->nid; ?>' class='btn btn-default'>任職管理</a>
            <?php print $content['links']['flag']['#links']['flag-create_work_title']['title']; ?>
        <?php endif; ?>
        <a class="btn btn-danger" data-toggle="modal" data-target="#myModal">勘誤回報 (提醒管理員)</a>
    </div>
    <?php
    $last_update = $variables['changed'];
    foreach ($variables['field_re_worktitle'] as $item) {
        $node = node_load($item['nid']);
        if ($node->revision_timestamp > $last_update) {
            $last_update = $node->revision_timestamp;
        }
    }
    ?>
    <blockquote class="blockquote-reverse pull-right">
        <footer>最後更新於: <?php print date('Y/m/d - H:i', $last_update); ?></footer>
    </blockquote>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">勘誤回報</h4>
                </div>
                <div class="modal-body">
                    <?php print ($contact_form); ?>
                </div>
            </div>
        </div>
    </div>
    </article>
<?php endif; ?>

<?php if ($view_mode == 'teaser'): ?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    <?php print render($title_prefix); ?>
    <h2><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php print render($title_suffix); ?>
    <div class="table-responsive">
        <table class="table table-bordered" id="basic-info">
            <tr>
                <?php if (isset($content['group_personal_data']['field_re_pic'])): ?>
                    <td rowspan='7' width="15%" class="text-center hidden-xs valign-middle">
                        <?php print render($content['group_personal_data']['field_re_pic']); ?>
                    </td>
                <?php endif; ?>
                <th>漢語拼音</th>
                <td colspan="2">
                    <?php print render($content['group_personal_data']['field_re_han_name']); ?>
                </td>
                <th>性別</th>
                <td colspan="2">
                    <?php print render($content['group_personal_data']['field_re_sex']); ?>
                </td>
            </tr>
            <tr>
                <th>年齡</th>
                <td>
                    <div class="field-items">
                        <div class="field-item">
                            <?php
                            if ($content['group_personal_data']['field_re_dead_year']) {
                                if ($content['group_personal_data']['field_re_birth_year'][0]['#markup']) {
                                    print $content['group_personal_data']['field_re_dead_year'][0]['#markup'] - $content['group_personal_data']['field_re_birth_year'][0]['#markup'];
                                }
                            } else {
                                if ($content['group_personal_data']['field_re_birth_year'][0]['#markup']) {
                                    print date('Y') - $content['group_personal_data']['field_re_birth_year'][0]['#markup'];
                                }
                            }
                            ?>
                        </div>
                    </div>
                </td>
                <th>出生</th>
                <td>
                    <div class="field-items">
                        <div class="field-item">
                            <?php
                            if (isset($content['group_personal_data']['field_re_birth_year'])) {
                                print $content['group_personal_data']['field_re_birth_year'][0]['#markup'];
                            }
                            if (isset($content['group_personal_data']['field_re_birth_year']) && isset($content['group_personal_data']['field_re_birth_month'])) {
                                print ' / ';
                            }
                            if (isset($content['group_personal_data']['field_re_birth_month'])) {
                                print $content['group_personal_data']['field_re_birth_month'][0]['#markup'];
                            }
                            ?>
                        </div>
                    </div>
                </td>
                <th>死亡</th>
                <td>
                    <div class="field-items">
                        <div class="field-item">
                            <?php
                            if (isset($content['group_personal_data']['field_re_dead_year'])) {
                                print $content['group_personal_data']['field_re_dead_year'][0]['#markup'];
                            }
                            if (isset($content['group_personal_data']['field_re_dead_year']) && isset($content['group_personal_data']['field_re_dead_month'])) {
                                print ' / ';
                            }
                            if (isset($content['group_personal_data']['field_re_dead_month'])) {
                                print $content['group_personal_data']['field_re_dead_month'][0]['#markup'];
                            }
                            ?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th>最高學歷</th>
                <td>
                    <?php print render($content['group_personal_data']['field_re_degree']); ?>
                </td>
                <th>留學經驗</th>
                <td>
                    <?php print render($content['group_personal_data']['field_re_abroad_exp']); ?>
                </td>
                <th>留學國家</th>
                <td>
                    <?php print render($content['group_personal_data']['field_re_abroad_country']); ?>
                </td>
            </tr>
            <tr>

                <th>民族</th>
                <td>
                    <?php print render($content['group_personal_data']['field_re_race']); ?>
                </td>
                <th>祖籍</th>
                <td>
                    <?php print render($content['taxonomy_vocabulary_2']); ?>
                    <?php print render($content['taxonomy_vocabulary_3']); ?>
                </td>
                <th>出生地</th>
                <td>
                    <?php print render($content['taxonomy_vocabulary_5']); ?>
                    <?php print render($content['taxonomy_vocabulary_6']); ?>
                </td>
            </tr>
            <tr>
                <th>政黨</th>
                <td>
                    <?php print render($content['group_personal_data']['field_re_party']); ?>
                </td>
                <th>專業頭銜</th>
                <td>
                    <?php print render($content['group_personal_data']['field_re_main_title']); ?>
                </td>
                <th>特殊關係</th>
                <td>
                    <?php print render($content['group_personal_data']['field_re_spe_relationship']); ?>
                </td>
            </tr>
            <tr>
                <th>入黨時間</th>
                <td>
                    <?php print render($content['group_personal_data']['field_re_participant_time']); ?>
                </td>
                <th>參加工作</th>
                <td>
                    <?php print render($content['group_personal_data']['field_re_work_time']); ?>
                </td>
                <th>共青團</th>
                <td>
                    <?php print render($content['group_personal_data']['field_re_cyc']); ?>
                </td>
            </tr>
            <tr>
                <th>備註</th>
                <td colspan=5>
                    <?php print render($content['group_personal_data']['field_re_remark']); ?>
                </td>
            </tr>
        </table>
    </div>
</article>
<?php endif; ?>