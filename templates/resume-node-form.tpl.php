<?php print drupal_render($form['title']); ?>
<?php print drupal_render($form['group_personal_data']['field_re_han_name']); ?>
<fieldset class="collapsible panel panel-primary form-wrapper collapse-processed" id="personal-panel">
    <legend class="panel-heading">
        <a href="#" class="panel-title fieldset-legend" data-toggle="collapse" data-target="#personal-panel > .collapse" aria-expanded="true"><span class="fieldset-legend-prefix element-invisible">隱藏</span>個人資料</a>
    </legend>
    <div class="panel-collapse fade collapse in" aria-expanded="true">
        <div class="panel-body">
            <div class="row">
                <?php
                print drupal_render($form['group_personal_data']['field_re_sex']);
                print drupal_render($form['group_personal_data']['field_re_race']);
                print drupal_render($form['group_personal_data']['field_re_birth_year']);
                print drupal_render($form['group_personal_data']['field_re_birth_month']);
                print drupal_render($form['group_personal_data']['field_re_dead_year']);
                print drupal_render($form['group_personal_data']['field_re_dead_month']);
                print drupal_render($form['taxonomy_vocabulary_2']);
                print drupal_render($form['taxonomy_vocabulary_3']);
                print drupal_render($form['taxonomy_vocabulary_5']);
                print drupal_render($form['taxonomy_vocabulary_6']);
                print drupal_render($form['group_personal_data']['field_re_degree']);
                print drupal_render($form['group_personal_data']['field_re_abroad_exp']);
                print drupal_render($form['group_personal_data']['field_re_abroad_country']);
                ?>
                <div class="col-sm-4 form-group">
                    <?php
                    print drupal_render($form['group_personal_data']['field_re_party']);
                    print drupal_render($form['group_personal_data']['field_re_participant_time']);
                    ?>
                </div>
                <div class="col-sm-4 form-group">
                    <?php
                    print drupal_render($form['group_personal_data']['field_re_main_title']);
                    print drupal_render($form['group_personal_data']['field_re_work_time']);
                    ?>
                </div>
                <div class="col-sm-4 form-group">
                    <?php
                    print drupal_render($form['group_personal_data']['field_re_spe_relationship']);
                    print drupal_render($form['group_personal_data']['field_re_cyc']);
                    ?>
                </div>
                <?php
                print drupal_render($form['group_personal_data']['field_re_relative']);
                print drupal_render($form['group_personal_data']['field_re_relativetext']);
                print drupal_render($form['group_personal_data']['field_re_remark']);
                print drupal_render($form['group_personal_data']['field_re_pic']);
                ?>
            </div>
            <div class="row"></div>
        </div>
</fieldset>
<?php
print drupal_render($form['field_re_relationship']);
print drupal_render($form['group_exprience']);
print drupal_render($form['language']);
print drupal_render_children($form);
?>
